package com.higgsup.model.entity;

import javax.persistence.*;

/**
 * Created by Higgsup-ThanhBC on 7/27/2018.
 */
@Entity
@Table(name = "relation_type")
public class RelationType {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
