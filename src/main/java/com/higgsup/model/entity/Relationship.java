package com.higgsup.model.entity;

import javax.persistence.*;

/**
 * Created by Higgsup-ThanhBC on 7/27/2018.
 */
@Entity
@Table(name = "relationship")
public class Relationship {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "from_character")
    private Long fromCharacter;

    @Column(name = "relation_type")
    private Long relationTypeId;

    @Column(name = "to_character")
    private Long toCharacter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFromCharacter() {
        return fromCharacter;
    }

    public void setFromCharacter(Long fromCharacter) {
        this.fromCharacter = fromCharacter;
    }

    public Long getRelationTypeId() {
        return relationTypeId;
    }

    public void setRelationTypeId(Long relationTypeId) {
        this.relationTypeId = relationTypeId;
    }

    public Long getToCharacter() {
        return toCharacter;
    }

    public void setToCharacter(Long toCharacter) {
        this.toCharacter = toCharacter;
    }
}
