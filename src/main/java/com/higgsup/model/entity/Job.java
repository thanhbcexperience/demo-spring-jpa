package com.higgsup.model.entity;

import javax.persistence.*;

/**
 * Created by Lucifer on 2018-07-15.
 */
@Entity
@Table(name = "job")
public class Job {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
