package com.higgsup.model.entity;

import javax.persistence.*;

/**
 * Created by Lucifer on 2018-07-15.
 */
@Entity
@Table(name = "main_character")
public class MainCharacter {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private Integer age;

    @ManyToOne
    @JoinColumn(name = "assets_status")
    private AssetsStatus assetsStatus;

    @Column(name = "is_hot")
    private Boolean isHot;

    @ManyToOne
    @JoinColumn(name = "job")
    private Job job;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public AssetsStatus getAssetsStatus() {
        return assetsStatus;
    }

    public void setAssetsStatus(AssetsStatus assetsStatus) {
        this.assetsStatus = assetsStatus;
    }

    public Boolean getHot() {
        return isHot;
    }

    public void setHot(Boolean hot) {
        isHot = hot;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }
}
