package com.higgsup.model.dto;

/**
 * Created by Higgsup-ThanhBC on 7/20/2018.
 */
public class MainCharacterDTO {
    private Long id;

    private String name;

    private Integer age;

    private AssetsStatusDTO assetsStatusDTO;

    private Boolean isHot;

    private JobDTO jobDTO;

    private RelationshipDTOs relationshipDTOs;

    public MainCharacterDTO() {
    }

    public MainCharacterDTO(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public AssetsStatusDTO getAssetsStatusDTO() {
        return assetsStatusDTO;
    }

    public void setAssetsStatusDTO(AssetsStatusDTO assetsStatusDTO) {
        this.assetsStatusDTO = assetsStatusDTO;
    }

    public Boolean getHot() {
        return isHot;
    }

    public void setHot(Boolean hot) {
        isHot = hot;
    }

    public JobDTO getJobDTO() {
        return jobDTO;
    }

    public void setJobDTO(JobDTO jobDTO) {
        this.jobDTO = jobDTO;
    }

    public RelationshipDTOs getRelationshipDTOs() {
        return relationshipDTOs;
    }

    public void setRelationshipDTOs(RelationshipDTOs relationshipDTOs) {
        this.relationshipDTOs = relationshipDTOs;
    }
}
