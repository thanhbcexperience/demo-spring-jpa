package com.higgsup.model.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Higgsup-ThanhBC on 7/27/2018.
 */
public class JobDTOs {
    private List<JobDTO> jobDTOS = new ArrayList<JobDTO>();

    public List<JobDTO> getJobDTOS() {
        return jobDTOS;
    }

    public void setJobDTOS(List<JobDTO> jobDTOS) {
        this.jobDTOS = jobDTOS;
    }
}
