package com.higgsup.model.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Higgsup-ThanhBC on 7/27/2018.
 */
public class RelationshipDTOs {
    private List<RelationshipDTO> relationshipDTOS = new ArrayList<RelationshipDTO>();

    public List<RelationshipDTO> getRelationshipDTOS() {
        return relationshipDTOS;
    }

    public void setRelationshipDTOS(List<RelationshipDTO> relationshipDTOS) {
        this.relationshipDTOS = relationshipDTOS;
    }
}
