package com.higgsup.model.dto;

/**
 * Created by Higgsup-ThanhBC on 7/27/2018.
 */
public class RelationshipDTO {
    private Long id;

    private String relationTypeName;

    private String toCharacterName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRelationTypeName() {
        return relationTypeName;
    }

    public void setRelationTypeName(String relationTypeName) {
        this.relationTypeName = relationTypeName;
    }

    public String getToCharacterName() {
        return toCharacterName;
    }

    public void setToCharacterName(String toCharacterName) {
        this.toCharacterName = toCharacterName;
    }
}
