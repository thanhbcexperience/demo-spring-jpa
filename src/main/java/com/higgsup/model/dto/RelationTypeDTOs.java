package com.higgsup.model.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Higgsup-ThanhBC on 7/27/2018.
 */
public class RelationTypeDTOs {
    private List<RelationTypeDTO> relationTypeDTOS = new ArrayList<RelationTypeDTO>();

    public List<RelationTypeDTO> getRelationTypeDTOS() {
        return relationTypeDTOS;
    }

    public void setRelationTypeDTOS(List<RelationTypeDTO> relationTypeDTOS) {
        this.relationTypeDTOS = relationTypeDTOS;
    }
}
