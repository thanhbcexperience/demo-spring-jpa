package com.higgsup.model.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Higgsup-ThanhBC on 7/20/2018.
 */
public class MainCharacterDTOs {
    private List<MainCharacterDTO> mainCharacterDTOS = new ArrayList<MainCharacterDTO>();

    public List<MainCharacterDTO> getMainCharacterDTOS() {
        return mainCharacterDTOS;
    }

    public void setMainCharacterDTOS(List<MainCharacterDTO> mainCharacterDTOS) {
        this.mainCharacterDTOS = mainCharacterDTOS;
    }
}
