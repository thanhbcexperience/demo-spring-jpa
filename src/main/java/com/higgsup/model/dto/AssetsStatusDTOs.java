package com.higgsup.model.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Higgsup-ThanhBC on 7/27/2018.
 */
public class AssetsStatusDTOs {
    private List<AssetsStatusDTO> assetsStatusDTOS = new ArrayList<AssetsStatusDTO>();

    public List<AssetsStatusDTO> getAssetsStatusDTOS() {
        return assetsStatusDTOS;
    }

    public void setAssetsStatusDTOS(List<AssetsStatusDTO> assetsStatusDTOS) {
        this.assetsStatusDTOS = assetsStatusDTOS;
    }
}
