package com.higgsup.model.repository;

import com.higgsup.model.entity.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Lucifer on 2018-07-15.
 */
public interface IJobRepository extends JpaRepository<Job, Long> {

    @Query("SELECT j FROM Job j")
    List<Job> findAllJobWithQueryAnnotation();

    @Query("SELECT j FROM Job j WHERE j.id = :jobId")
    Job findByJobIdQueryAnnotation(@Param("jobId") Long jobId);

//    @Query("SELECT j FROM Job j WHERE j.id = ?1")
//    Job findByJobIdQueryAnnotation(Long jobId);
}
