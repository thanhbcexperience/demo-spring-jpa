package com.higgsup.model.repository;

import com.higgsup.model.entity.AssetsStatus;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Lucifer on 2018-07-15.
 */
public interface IAssetStatusRepository extends JpaRepository<AssetsStatus, Long> {
    AssetsStatus findByName(String name);
}
