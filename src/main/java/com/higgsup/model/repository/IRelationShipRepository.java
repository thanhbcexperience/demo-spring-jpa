package com.higgsup.model.repository;

import com.higgsup.model.entity.Relationship;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Higgsup-ThanhBC on 7/27/2018.
 */
public interface IRelationShipRepository extends JpaRepository<Relationship, Long>{
    List<Relationship> findByFromCharacter(Long id);
}
