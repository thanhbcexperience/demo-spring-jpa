package com.higgsup.model.repository.custom.impl;

import com.higgsup.model.repository.custom.CustomMainCharacterRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Higgsup-ThanhBC on 7/30/2018.
 */
public class MainCharacterRepositoryImpl implements CustomMainCharacterRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Object[]> selectUserNativeQuery(Long userId) {
        String sql = "SELECT * FROM main_character";

        Query query =  entityManager.createNativeQuery(sql);
//        query.setParameter(1, userId);
        return query.getResultList();
    }
}
