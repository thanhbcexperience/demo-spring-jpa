package com.higgsup.model.repository.custom;

import java.util.List;

/**
 * Created by Higgsup-ThanhBC on 7/30/2018.
 */
// If select contains single expression and it's an entity, then result is that entity
// If select contains single expression and it's a primitive, then result is that primitive
// If select contains multiple expressions, then result is Object[] containing the corresponding primitives/entities
public interface CustomMainCharacterRepository {

    List<Object[]> selectUserNativeQuery(Long userId);

}
