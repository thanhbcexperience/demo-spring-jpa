package com.higgsup.model.repository;

import com.higgsup.model.entity.RelationType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Higgsup-ThanhBC on 7/27/2018.
 */
public interface IRelationTypeRepository extends JpaRepository<RelationType, Long>{
    RelationType findByName(String relationTypeName);
}
