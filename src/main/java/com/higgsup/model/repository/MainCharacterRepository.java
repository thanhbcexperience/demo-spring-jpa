package com.higgsup.model.repository;

import com.higgsup.model.entity.MainCharacter;
import com.higgsup.model.repository.custom.CustomMainCharacterRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Lucifer on 2018-07-15.
 */

public interface MainCharacterRepository extends JpaRepository<MainCharacter, Long>, CustomMainCharacterRepository {
    List<MainCharacter> findByIsHotTrue();

    MainCharacter findByName(String name);
}
