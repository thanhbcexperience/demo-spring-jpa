package com.higgsup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Lucifer on 2018-07-15.
 */
@SpringBootApplication
public class TrueStoryApplication {
    public static void main(String[] args) {
        SpringApplication.run(TrueStoryApplication.class, args);
    }
}
