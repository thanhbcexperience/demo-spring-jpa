package com.higgsup.controller;

import com.higgsup.model.dto.JobDTO;
import com.higgsup.model.dto.MainCharacterDTO;
import com.higgsup.model.dto.MainCharacterDTOs;
import com.higgsup.service.IMainCharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Lucifer on 2018-07-15.
 */
@RestController
@RequestMapping("/characters")
public class MainCharacterController {
    @Autowired
    private IMainCharacterService mainCharacterService;

    @RequestMapping(method = RequestMethod.GET)
    public String buildAStory() {
        return mainCharacterService.buildStory();
    }

    @RequestMapping(path = "/all", method = RequestMethod.GET)
    private MainCharacterDTOs getAllCharacters() {
        return mainCharacterService.getAllCharacters();
    }

    @RequestMapping(path = "/{userId}", method = RequestMethod.GET)
    private MainCharacterDTO getUserById(@PathVariable Long userId) {
        return mainCharacterService.getUserById(userId);
    }

    @RequestMapping(path = "/{userId}", method = RequestMethod.PUT)
    private MainCharacterDTO becomeABusinessman(@PathVariable Long userId) {
        return mainCharacterService.becomeABusinessman(userId);
    }

    @RequestMapping(path = "/is-hot", method = RequestMethod.GET)
    private MainCharacterDTOs getHotUser() {
        return mainCharacterService.getHotUser();
    }

    @RequestMapping(path = "/native/{userId}", method = RequestMethod.GET)
    private MainCharacterDTOs getUserByNativeQuery(@PathVariable Long userId) {
        return mainCharacterService.getUserByNativeQuery(userId);
    }
}
