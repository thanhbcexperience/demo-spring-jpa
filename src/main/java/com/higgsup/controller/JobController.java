package com.higgsup.controller;

import com.higgsup.model.dto.JobDTO;
import com.higgsup.model.dto.JobDTOs;
import com.higgsup.service.IJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Higgsup-ThanhBC on 7/30/2018.
 */
@RestController
@RequestMapping("/jobs")
public class JobController {
    @Autowired
    private IJobService jobService;

    @RequestMapping(method = RequestMethod.GET)
    public JobDTOs getAllJob() {
        return jobService.getAllJob();
    }

    @RequestMapping(path = "/{jobId}", method = RequestMethod.GET)
    public JobDTO getJobById(@PathVariable Long jobId) {
        return jobService.getJobById(jobId);
    }
}
