package com.higgsup.service;

import com.higgsup.model.dto.JobDTO;
import com.higgsup.model.dto.MainCharacterDTO;
import com.higgsup.model.dto.MainCharacterDTOs;

/**
 * Created by Lucifer on 2018-07-15.
 */
public interface IMainCharacterService {
    String buildStory();

    MainCharacterDTOs getAllCharacters();

    MainCharacterDTO getUserById(Long userId);

    MainCharacterDTO becomeABusinessman(Long userId);

    MainCharacterDTOs getHotUser();

    MainCharacterDTOs getUserByNativeQuery(Long userId);
}
