package com.higgsup.service;

import com.higgsup.model.dto.JobDTO;
import com.higgsup.model.dto.JobDTOs;


/**
 * Created by Higgsup-ThanhBC on 7/30/2018.
 */
public interface IJobService {
    JobDTOs getAllJob();

    JobDTO getJobById(Long jobId);
}
