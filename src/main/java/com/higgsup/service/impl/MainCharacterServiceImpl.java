package com.higgsup.service.impl;

import com.higgsup.model.dto.*;
import com.higgsup.model.entity.*;
import com.higgsup.model.repository.*;
import com.higgsup.service.IMainCharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Lucifer on 2018-07-15.
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MainCharacterServiceImpl implements IMainCharacterService {
    @Autowired
    private IAssetStatusRepository assetStatusRepository;

    @Autowired
    private IJobRepository jobRepository;

    @Autowired
    private MainCharacterRepository mainCharacterRepository;

    @Autowired
    private IRelationTypeRepository relationTypeRepository;

    @Autowired
    private IRelationShipRepository relationShipRepository;

    @Override
    public String buildStory() {

        //Tạo dữ liệu cho trạng thái tài sản
        createNewAssetsStatus("Poor");
        createNewAssetsStatus("Stable");
        createNewAssetsStatus("Rich");
        createNewAssetsStatus("Supper Rich");

        //Tạo dữ liệu cho công việc
        createNewJob("Unemploy");
        createNewJob("Shipper");
        createNewJob("Employee");
        createNewJob("Actor");
        createNewJob("Businessman");
        createNewJob("Developer");

        //Tạo nhân vật theo cốt truyện
        createNewCharacter("Walter White", 74, 4, false, 5);
        createNewCharacter("Elly Nude", 24, 2, true, 3);
        createNewCharacter("Sam Good Girl", 24, 3, false, 5);
        createNewCharacter("David Poor Man", 26, 1, true, 1);

        //Tạo cảm xúc cho các nhân vật
        createNewRelationType("Boy Friend");
        createNewRelationType("Girl Friend");
        createNewRelationType("Boss");
        createNewRelationType("Friend");
        createNewRelationType("Lover");
        createNewRelationType("Crush");
        createNewRelationType("Enemy");
        createNewRelationType("Husband");
        createNewRelationType("Wife");

        //Dựa trên các cảm xúc đó mà tạo dự liệu mối quan hệ giữa các nhân vật
        MainCharacter david = mainCharacterRepository.findByName("David Poor Man");
        MainCharacter elly = mainCharacterRepository.findByName("Elly Nude");
        MainCharacter sam = mainCharacterRepository.findByName("Sam Good Girl");
        MainCharacter walter = mainCharacterRepository.findByName("Walter White");
        createRelationShip(elly.getId(),"Girl Friend",  david.getId());
        createRelationShip(david.getId(),"Boy Friend", elly.getId());
        createRelationShip(sam.getId(),"Friend", elly.getId());
        createRelationShip(walter.getId(),"Boss", sam.getId());

        return "Story Begin!";
    }

    @Override
    public MainCharacterDTOs getAllCharacters() {
        MainCharacterDTOs mainCharacterDTOs = new MainCharacterDTOs();
        Iterable<MainCharacter> mainCharacters = mainCharacterRepository.findAll();
        for (MainCharacter mainCharacter : mainCharacters) {
            MainCharacterDTO mainCharacterDTO = generateMainCharacterDTOFromMainCharacter(mainCharacter);
            mainCharacterDTOs.getMainCharacterDTOS().add(mainCharacterDTO);

        }
        return mainCharacterDTOs;
    }

    @Override
    public MainCharacterDTO getUserById(Long userId) {
        MainCharacter mainCharacter = mainCharacterRepository.findOne(userId);
        return generateMainCharacterDTOFromMainCharacter(mainCharacter);
    }

    @Override
    public MainCharacterDTO becomeABusinessman(Long userId) {
        MainCharacter mainCharacter = mainCharacterRepository.findOne(userId);
        mainCharacter.setJob(jobRepository.findOne((long) 5));
        mainCharacterRepository.save(mainCharacter);
        return generateMainCharacterDTOFromMainCharacter(mainCharacter);
    }

    @Override
    public MainCharacterDTOs getHotUser() {
        List<MainCharacter> mainCharacters = mainCharacterRepository.findByIsHotTrue();
        MainCharacterDTOs mainCharacterDTOs = new MainCharacterDTOs();
        for (MainCharacter mainCharacter : mainCharacters) {
            MainCharacterDTO mainCharacterDTO = generateMainCharacterDTOFromMainCharacter(mainCharacter);
            mainCharacterDTOs.getMainCharacterDTOS().add(mainCharacterDTO);
        }

        return mainCharacterDTOs;
    }

    @Override
    public MainCharacterDTOs getUserByNativeQuery(Long userId) {
        MainCharacterDTOs mainCharacterDTOs = new MainCharacterDTOs();
        List<Object[]> objects = mainCharacterRepository.selectUserNativeQuery(userId);

        for (Object[] object : objects) {
            MainCharacterDTO mainCharacterDTO = new MainCharacterDTO((String) object[3], (Integer) object[1]);
            mainCharacterDTOs.getMainCharacterDTOS().add(mainCharacterDTO);
        }
        return mainCharacterDTOs;
    }

    private MainCharacterDTO generateMainCharacterDTOFromMainCharacter(MainCharacter mainCharacter) {
        MainCharacterDTO mainCharacterDTO = new MainCharacterDTO();
        RelationshipDTO relationshipDTO = new RelationshipDTO();

        JobDTO jobDTO = new JobDTO();
        jobDTO.setId(mainCharacter.getJob().getId());
        jobDTO.setName(mainCharacter.getJob().getName());

        AssetsStatusDTO assetsStatusDTO = new AssetsStatusDTO();
        assetsStatusDTO.setId(mainCharacter.getAssetsStatus().getId());
        assetsStatusDTO.setName(mainCharacter.getAssetsStatus().getName());

        RelationshipDTOs relationshipDTOs = new RelationshipDTOs();
        List<Relationship> relationships = relationShipRepository.findByFromCharacter(mainCharacter.getId());

        if (relationships.size() > 0) {
            for (Relationship relationship : relationships) {

                MainCharacter toCharacter = mainCharacterRepository.findOne(relationship.getToCharacter());
                RelationType relationType = relationTypeRepository.findOne(relationship.getRelationTypeId());

                relationshipDTO.setId(relationship.getId());
                relationshipDTO.setRelationTypeName(relationType.getName());
                relationshipDTO.setToCharacterName(toCharacter.getName());
                relationshipDTOs.getRelationshipDTOS().add(relationshipDTO);
            }
        }

        mainCharacterDTO.setId(mainCharacter.getId());
        mainCharacterDTO.setName(mainCharacter.getName());
        mainCharacterDTO.setAge(mainCharacter.getAge());
        mainCharacterDTO.setJobDTO(jobDTO);
        mainCharacterDTO.setAssetsStatusDTO(assetsStatusDTO);
        mainCharacterDTO.setHot(mainCharacter.getHot());
        mainCharacterDTO.setRelationshipDTOs(relationshipDTOs);
        return mainCharacterDTO;

    }

    private void createNewCharacter(String name, Integer age, long assetsStatus, Boolean isHot, long job) {
        MainCharacter mainCharacter = new MainCharacter();
        mainCharacter.setName(name);
        mainCharacter.setAge(age);
        mainCharacter.setAssetsStatus(assetStatusRepository.findOne(assetsStatus));
        mainCharacter.setHot(isHot);
        mainCharacter.setJob(jobRepository.findOne(job));
        mainCharacterRepository.save(mainCharacter);
    }

    private void createNewAssetsStatus(String name) {
        AssetsStatus assetsStatus = new AssetsStatus();
        assetsStatus.setName(name);
        assetStatusRepository.save(assetsStatus);
    }


    private void createNewJob(String name) {
        Job job = new Job();
        job.setName(name);
        jobRepository.save(job);
    }

    private void createNewRelationType(String name) {
        RelationType relationType = new RelationType();
        relationType.setName(name);
        relationTypeRepository.save(relationType);
    }

    private void createRelationShip(Long from, String relationTypeName, Long to) {
        Relationship relationship = new Relationship();
        relationship.setFromCharacter(from);
        relationship.setRelationTypeId(relationTypeRepository.findByName(relationTypeName).getId());
        relationship.setToCharacter(to);
        relationShipRepository.save(relationship);
    }

}
