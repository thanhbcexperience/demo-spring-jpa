package com.higgsup.service.impl;

import com.higgsup.model.dto.JobDTO;
import com.higgsup.model.dto.JobDTOs;
import com.higgsup.model.entity.Job;
import com.higgsup.model.repository.IJobRepository;
import com.higgsup.service.IJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Higgsup-ThanhBC on 7/30/2018.
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class JobServiceImpl implements IJobService{

    @Autowired
    private IJobRepository jobRepository;

    @Override
    public JobDTOs getAllJob() {
        JobDTOs jobDTOs = new JobDTOs();
        List<Job> jobList = jobRepository.findAllJobWithQueryAnnotation();
        for (Job job : jobList) {
            JobDTO jobDTO = new JobDTO();
            jobDTO.setId(job.getId());
            jobDTO.setName(job.getName());
            jobDTOs.getJobDTOS().add(jobDTO);
        }
        return jobDTOs;
    }

    @Override
    public JobDTO getJobById(Long jobId) {
        Job job = jobRepository.findByJobIdQueryAnnotation(jobId);
        JobDTO jobDTO = new JobDTO();
        jobDTO.setName(job.getName());
        return jobDTO;
    }
}
